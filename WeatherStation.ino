#include "DHT.h"
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

class WeatherDataPackage {
  public:
  String userName;
  String updatedAt;
  float temperature;
  float humidity;
  float lightIntensity;
  boolean precipitation;
  float windSpeed;
  
  WeatherDataPackage(){ 
    //the constructor 
  }
};

String deviceId = "59b10822abe4ea000475153b";

WeatherDataPackage wdp;

HTTPClient http;

// Connection constants
const char* ssid = "NelisWifi";
const char* password = "gekwachtwoord";
const String httpPostUrl = "http://iot-open-server.herokuapp.com/data";
const String apiToken = "dc58df866e3fd1e662bfeb7a";

// DHT constants
#define DHTPIN D4
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);

// Display constants
const int8_t RST_PIN = D2;
const int8_t CE_PIN = D1;
const int8_t DC_PIN = D6;
const int8_t BL_PIN = D0;
Adafruit_PCD8544 display = Adafruit_PCD8544(DC_PIN, CE_PIN, RST_PIN);

//Anemometer Technical Variables
//The following variables correspond to the anemometer sold by Adafruit, but could be modified to fit other anemometers.
float voltageMin = .61; // Mininum output voltage from anemometer in mV.
float windSpeedMin = 0; // Wind speed in meters/sec corresponding to minimum voltage

float voltageMax = 2.0; // Maximum output voltage from anemometer in mV.
float windSpeedMax = 32; // Wind speed in meters/sec corresponding to maximum voltage

//Setup Variables
float voltageConversionConstant = .004882814; //This constant maps the value provided from the analog read function, which ranges from 0 to 1023, to actual voltage, which ranges from 0V to 5V
const int windSensorPin = A0; //Defines the pin that the anemometer output is connected to
int sensorValue = 0; //Variable stores the value direct from the analog pin
float sensorVoltage = 0; //Variable that stores the voltage (in Volts) from the anemometer being sent to the analog pin
float windSpeed = 0; // Wind speed in meters per second (m/s)

// Actuator
const String actuatorPostUrl = "http://toinfinity.nl/smart-things/proxy.php?token=4d26b734-92454b3409b14ccf9f0b233";
const String actuatorDeviceId = "80063AEE437056A259250C97FB7026CA17B51756";
bool triggered = 0;

void setup() {
  Serial.begin(115200);
  
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Setup DHT.
  dht.begin();

  // Setup temperature sensor
  pinMode(D3, INPUT);
  
  // Turn LCD backlight on
  pinMode(BL_PIN, OUTPUT);
  digitalWrite(BL_PIN, HIGH);

  // Configure LCD
  display.begin();
  display.setContrast(60);  // Adjust for your display
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.clearDisplay();

  display.println("Weerstation");
  display.println("Nelis");
  display.display();
  
  connectToWifiNetwork();
}

void loop() {
  // Read values from DHT.
  float humidity = getHumidity();
  float temperature = getTemperature();
  float heatIndex = dht.computeHeatIndex(temperature, humidity, false);

  // Read rain sensor.
  boolean isRaining = readSensor(D3);

  if(temperature > 24) {
    toggleActuator("1");
  } else {
    toggleActuator("0");
  }

  // Read wind sensor.
  windSpeed = readWindSensor(windSensorPin);

  // Create json to send to the server.
  StaticJsonBuffer<400> jsonBuffer;
  JsonObject& jsonRoot = jsonBuffer.createObject();
  jsonRoot["token"] = apiToken;
  
  JsonArray& data = jsonRoot.createNestedArray("data");

  JsonObject& humidityObject = jsonBuffer.createObject();
  humidityObject["key"] = "humidity";
  humidityObject["value"] = humidity;

  JsonObject& temperatureObject = jsonBuffer.createObject();
  temperatureObject["key"] = "temperature";
  temperatureObject["value"] = temperature;

  JsonObject& rainObject = jsonBuffer.createObject();
  rainObject["key"] = "rain";
  rainObject["value"] = isRaining;
  
  JsonObject& windObject = jsonBuffer.createObject();
  windObject["key"] = "wind";
  windObject["value"] = windSpeed;

  data.add(humidityObject);
  data.add(temperatureObject);
  data.add(rainObject);
  data.add(windObject);

  String dataToSend;
  jsonRoot.printTo(dataToSend);

  Serial.println(dataToSend);

  printData(heatIndex, isRaining, windSpeed);
  postData(dataToSend);

  delay(2000);
}

float getTemperature() {
  float temperature = 0.00;
  
  while(temperature == 0.00 || isnan(temperature)) {
    temperature = dht.readTemperature();
    yield();
  }

  return temperature;
}

float getHumidity() {
  float humidity = 0.00;

  while(humidity == 0.00 || isnan(humidity)) {
    humidity = dht.readHumidity();
    yield();
  }

  return humidity;
}

boolean readSensor(int pin) {
  return !(digitalRead(pin));
};

float readWindSensor(int pin) {
  sensorValue = analogRead(pin); //Get a value between 0 and 1023 from the analog pin connected to the anemometer
  sensorVoltage = sensorValue * voltageConversionConstant; //Convert sensor value to actual voltage
  
  //Convert voltage value to wind speed using range of max and min voltages and wind speed for the anemometer
  if (sensorVoltage <= voltageMin){
   return 0; //Check if voltage is below minimum value. If so, set wind speed to zero.
  }else {
    return (sensorVoltage - voltageMin)*windSpeedMax/(voltageMax - voltageMin); //For voltages above minimum value, use the linear relationship to calculate wind speed.
  }
}

void printData(float heatIndex, boolean rain, float wind) {
  display.clearDisplay();
  display.print("Temp: ");
  display.print(heatIndex);
  display.println(" C");
  if(rain) {
    display.println("Regen: ja");
  } else {
    display.println("Regen: nee");
  }
  display.print("WindSnelheid: ");
  display.println(wind);
  display.display();
}

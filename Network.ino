void connectToWifiNetwork() {
  display.clearDisplay();
  display.print("Connecting");
  Serial.print("Connecting");
  WiFi.begin(ssid, password);
  display.display();
  
  // Wait for connection
  while ( WiFi.status() != WL_CONNECTED ) {
    delay(500);
    Serial.print(".");
    display.print(".");
    display.display();
  }

  Serial.print("\nConnected to: ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("");

  display.clearDisplay();
  display.print("Connected to: ");
  display.println(ssid);
  display.print("IP address: ");
  display.println(WiFi.localIP());
  display.println("");
  display.display();
}

void postData(String stringToPost) {
  if(WiFi.status()== WL_CONNECTED){
    http.begin(httpPostUrl);
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST(stringToPost);
    String payload = http.getString();
  
    Serial.println("Post Success - Code: ");
    Serial.println(httpCode);
    http.end();
  } else {
    Serial.println("Wifi connection failed, retrying.");   
    connectToWifiNetwork();
  }
}

void toggleActuator(String state) {
  if (WiFi.status() == WL_CONNECTED) {
    StaticJsonBuffer<400> jsonBuffer;
    JsonObject& jsonRoot = jsonBuffer.createObject();

    jsonRoot["method"] = "passthrough";
    jsonRoot["state"] = state;

    JsonObject& paramsObject = jsonBuffer.createObject();
    paramsObject["deviceId"] = actuatorDeviceId;
    paramsObject["requestData"] = "{\"system\":{\"set_relay_state\":{\"state\":" + state + "}}}";

    jsonRoot["params"] = paramsObject;

    String stringToPost;
    jsonRoot.printTo(stringToPost);

    Serial.println("Setting actuator state to " + state + "....");
    http.begin(actuatorPostUrl + "&state=" + state);
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST(stringToPost);
    http.end();
    triggered = false;
  } else {
    Serial.println("Wifi connection failed, retrying.");
    connectToWifiNetwork();
  }
}

